package com.example.homework2.service;

import com.example.homework2.model.entity.Invoice;
import com.example.homework2.model.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {

    Invoice  addNewInvoice(InvoiceRequest invoiceRequest);

    List<Invoice> getInvoice();

    Invoice  getInvoiceById(Integer invoiceId);

    public boolean deleteInvoiceById(Integer invoiceId);

    Integer updateInvoice(InvoiceRequest invoiceRequest, Integer invoiceId);
}
