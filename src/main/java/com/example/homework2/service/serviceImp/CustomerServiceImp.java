package com.example.homework2.service.serviceImp;

import com.example.homework2.model.entity.Customer;
import com.example.homework2.model.request.CustomerRequest;
import com.example.homework2.repository.CustomerRepository;
import com.example.homework2.service.CustomerService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCusomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerById(customerId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        Integer   customerId = customerRepository.addNewCustomer(customerRequest);
                return customerId;
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customerId) {
        Integer updateId = customerRepository.updateCustomer(customerRequest,customerId);
        return updateId;
    }
}
