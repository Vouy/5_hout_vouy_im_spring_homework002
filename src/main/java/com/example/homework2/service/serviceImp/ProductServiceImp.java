package com.example.homework2.service.serviceImp;

import com.example.homework2.model.entity.Product;
import com.example.homework2.model.request.ProductRequest;
import com.example.homework2.repository.ProductRepository;
import com.example.homework2.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProducts();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductById(productId);
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        return productRepository.addNewProduct(productRequest);
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer productId) {
        return productRepository.updateProduct(productRequest,productId);
    }


}
