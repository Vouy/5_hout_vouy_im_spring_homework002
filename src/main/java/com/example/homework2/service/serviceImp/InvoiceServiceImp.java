package com.example.homework2.service.serviceImp;

import com.example.homework2.model.entity.Invoice;
import com.example.homework2.model.request.InvoiceRequest;
import com.example.homework2.repository.InvoiceRepository;
import com.example.homework2.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {

    private  final InvoiceRepository  invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public Invoice addNewInvoice(InvoiceRequest invoiceRequest) {

        Integer invoiceID = invoiceRepository.addInvoice(invoiceRequest);

        for (Integer  productId:invoiceRequest.getProduct_ids()){
            invoiceRepository.addInvoiceDetail(invoiceID,productId);
        }

        return invoiceRepository.getInvoiceById(invoiceID);
    }

    @Override
    public List<Invoice> getInvoice() {
        return invoiceRepository.getInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public boolean deleteInvoiceById(Integer invoiceId) {
        return invoiceRepository.deleteInvoiceById(invoiceId);
    }

    @Override
    public Integer updateInvoice(InvoiceRequest invoiceRequest, Integer invoiceId) {
        invoiceRepository.deleteProductBeforeUpdate(invoiceId);
        for (Integer  productId:invoiceRequest.getProduct_ids()){
            invoiceRepository.addInvoiceDetail(invoiceId,productId);
        }
        return invoiceRepository.updateInvoice(invoiceRequest,invoiceId);
    }



}
