package com.example.homework2.service;

import com.example.homework2.model.entity.Customer;
import com.example.homework2.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomer();

    public Customer getCustomerById(Integer customerId);

    public boolean deleteCustomerById(Integer customerId);

    Integer addNewCustomer(CustomerRequest customerRequest);

    Integer updateCustomer(CustomerRequest customerRequest , Integer customerId);
}
