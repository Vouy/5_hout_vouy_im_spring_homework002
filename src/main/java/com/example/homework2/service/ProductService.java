package com.example.homework2.service;

import com.example.homework2.model.entity.Product;
import com.example.homework2.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();

    public Product getProductById(Integer productId);

    public boolean deleteProductById(Integer productId);

    Integer  addNewProduct(ProductRequest productRequest);

    Integer  updateProduct(ProductRequest productRequest, Integer productId);

}
