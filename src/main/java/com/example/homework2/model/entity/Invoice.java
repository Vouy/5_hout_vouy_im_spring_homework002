package com.example.homework2.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Invoice {
    private  Integer  id;
    private  String  invoice_date;
    private  Customer customer;
    private List<Product> productList;
}
