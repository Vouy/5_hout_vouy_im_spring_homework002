package com.example.homework2.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Customer {
    private  Integer id;
    private  String cus_name;
    private  String cus_address;
    private  String cus_phone;
}
