package com.example.homework2.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class InvoiceRequest {
    private  Timestamp date ;
    private  Integer  customer_id;
    private  ArrayList<Integer>  product_ids ;
}
