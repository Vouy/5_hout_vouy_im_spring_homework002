package com.example.homework2.controller;

import com.example.homework2.model.entity.Invoice;
import com.example.homework2.model.entity.Product;
import com.example.homework2.model.request.InvoiceRequest;
import com.example.homework2.model.response.InvoiceResponse;
import com.example.homework2.service.InvoiceService;
import com.example.homework2.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {
    private  final InvoiceService invoiceService;
    private  final ProductService productService;

    public InvoiceController(InvoiceService invoiceService, ProductService productService) {
        this.invoiceService = invoiceService;
        this.productService = productService;
    }

    @PostMapping("/add-invoice")
    @Operation(summary = "Add Invoice")
    public ResponseEntity<?> addNewInvoice (@RequestBody InvoiceRequest invoiceRequest){
        for (Integer  productId:invoiceRequest.getProduct_ids()){
           Product p  =   productService.getProductById(productId);
           if(p == null){
               return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This Product ID "+productId+" Is Not Available");
           }
        }
        Invoice sotreInvoiceId = invoiceService.addNewInvoice(invoiceRequest);
        if(sotreInvoiceId != null){
            InvoiceResponse<Invoice> response = InvoiceResponse.<Invoice>builder()
                    .message("Add Invoice Successfully")
                    .payload(invoiceService.addNewInvoice(invoiceRequest))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return  null;
    }

    @GetMapping("/get-all-invoice")
    @Operation(summary = "Get All Invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getInvoice(){
        InvoiceResponse<List<Invoice>> response = InvoiceResponse.<List<Invoice>>builder()
                .message("Get All Invoice Successfully")
                .payload(invoiceService.getInvoice())
                .httpStatus(true)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-invoice-by-id/{id}")
    @Operation(summary = "Get Invoice By Id")
    public ResponseEntity<InvoiceResponse<Invoice>>  getInvoiceById(@PathVariable("id") Integer invoiceId){
        if(invoiceService.getInvoiceById(invoiceId) != null){
            InvoiceResponse<Invoice> response = InvoiceResponse.<Invoice>builder()
                    .message("Get Invoice By Id Successfully")
                    .payload(invoiceService.getInvoiceById(invoiceId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            InvoiceResponse<Invoice> response = InvoiceResponse.<Invoice>builder()
                    .message("Sorry you can't get Invoice by this id ")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }

    }

    @DeleteMapping("/delete-invoice-by-id/{id}")
    @Operation(summary = "Delete Invoice By ID")
     public ResponseEntity<InvoiceResponse<String>> deleteInvoiceById(@PathVariable("id") Integer invoiceId){
        InvoiceResponse<String> response = null;
        if(invoiceService.deleteInvoiceById(invoiceId) ==  true){
            response = InvoiceResponse.<String>builder()
                    .message("Delete Invoice Successfully")
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }else{
            response = InvoiceResponse.<String>builder()
                    .message("Sorry you can't delete ")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }

    }

    @PutMapping("/update-invoice-by-id/{id}")
    @Operation(summary = "Update Invoice By ID")
    public ResponseEntity<InvoiceResponse<Invoice>> updateInvoice(@RequestBody InvoiceRequest invoiceRequest ,@PathVariable("id") Integer invoiceId){
        Integer updateInvoice =  invoiceService.updateInvoice(invoiceRequest,invoiceId);
        if(updateInvoice != null){
            InvoiceResponse<Invoice> response = InvoiceResponse.<Invoice>builder()
                    .message("Update Invoice By Id Successfully")
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            InvoiceResponse<Invoice> response = InvoiceResponse.<Invoice>builder()
                    .message("Sorry you can't Update ")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }

    }
}

