package com.example.homework2.controller;

import com.example.homework2.model.entity.Customer;
import com.example.homework2.model.request.CustomerRequest;
import com.example.homework2.model.response.CustomerResponse;
import com.example.homework2.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    private  final CustomerService customerService ;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get-all-customer")
    @Operation(summary = "Get All Customer")
    public ResponseEntity<CustomerResponse<List<Customer>>>  getAllCustomer(){

        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .payload(customerService.getAllCustomer())
                .message("Get Data Successfully")
                .httpStatus(true)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response) ;
    }

    @GetMapping("/get-customer-by-id/{id}")
    @Operation(summary = "Get Customer By Id")
    public ResponseEntity<CustomerResponse<Customer>>  getCustomerById(@PathVariable("id") Integer customerId){
        if( customerService.getCustomerById(customerId) != null){
            CustomerResponse<Customer> response  = CustomerResponse.<Customer>builder()
                    .message("Get Data By ID Successfully")
                    .payload(customerService.getCustomerById(customerId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            CustomerResponse<Customer> response  = CustomerResponse.<Customer>builder()
                    .message("This id is search not found !")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }

    }
    
    @DeleteMapping("/delete-customer-by-id/{id}")
    @Operation(summary = "Delete Customer By Id")
    public ResponseEntity<CustomerResponse<String>>  deleteCustomerById(@PathVariable("id") Integer customerId){
        CustomerResponse<String> response = null;
        if( customerService.deleteCustomerById(customerId) ==  true){
             response  = CustomerResponse.<String>builder()
                    .message("Delete Data Successfully")
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
             response  = CustomerResponse.<String>builder()
                    .message("This id is search not found , So u can't delete !")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }

    }

    @PostMapping("/add-new-customer")
    @Operation(summary = "Add New Customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        Integer storeCustomerId = customerService.addNewCustomer(customerRequest);
        if(storeCustomerId != null){
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Add Successfully")
                    .payload(customerService.getCustomerById(storeCustomerId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }else {
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Sorry You Add Data Fail")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }
    }

    @PutMapping("/update-customer-by-id/{id}")
    @Operation(summary = "Update Customer By Id")
    public  ResponseEntity<CustomerResponse<Customer>> updateCustomerById(@RequestBody CustomerRequest customerRequest, Integer customerId){
        Integer updateCustomerId =  customerService.updateCustomer(customerRequest,customerId);
        if(updateCustomerId != null){
            CustomerResponse<Customer> response  =  CustomerResponse.<Customer>builder()
                    .message("Update Successfully")
                    .payload(customerService.getCustomerById(updateCustomerId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }else{
            CustomerResponse<Customer> response  =  CustomerResponse.<Customer>builder()
                    .message("Sorry you can't update this Id ")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }
    }

}
