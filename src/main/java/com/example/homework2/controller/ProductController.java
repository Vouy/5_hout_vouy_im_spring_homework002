package com.example.homework2.controller;

import com.example.homework2.model.entity.Customer;
import com.example.homework2.model.entity.Product;
import com.example.homework2.model.request.CustomerRequest;
import com.example.homework2.model.request.ProductRequest;
import com.example.homework2.model.response.CustomerResponse;
import com.example.homework2.model.response.ProductResponse;
import com.example.homework2.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-product/{id}")
    @Operation(summary = "Get All Product")
    public ResponseEntity<ProductResponse<List<Product>>>  getAllProducts(){

        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .message("Get Data Successfully")
                .payload(productService.getAllProduct())
                .httpStatus(true)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return  ResponseEntity.ok(response) ;
    }

    @GetMapping("/get-product-by-id/{id}")
    @Operation(summary = "Get Product By Id")
    public  ResponseEntity<ProductResponse<Product>>  getProductById(@PathVariable("id") Integer productId){
        if(productService.getProductById(productId) != null){
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Get Product By Id Successfully")
                    .payload(productService.getProductById(productId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }else{
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Sorry you can't get this Id")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }
    }

    @DeleteMapping("/delete-product-by-id/{id}")
    @Operation(summary = "Delete Products By Id")
    public  ResponseEntity<ProductResponse<String>>  deleteProductById(@PathVariable("id") Integer productId){
        ProductResponse<String> response =  null;
        if(productService.deleteProductById(productId) == true){
            response = ProductResponse.<String>builder()
                    .message("Delete Product Successfully")
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }else{
             response = ProductResponse.<String>builder()
                    .message("Sorry you can't Delete this Id")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }
    }

    @PostMapping("/add-new-product")
    @Operation(summary = "Add New Product")
    public ResponseEntity<ProductResponse<Product>> addNewCustomer(@RequestBody ProductRequest productRequest){
        Integer storeProductId = productService.addNewProduct(productRequest);
        if(storeProductId != null){
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Add Successfully")
                    .payload(productService.getProductById(storeProductId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }
        return null;

    }

    @PutMapping("/update-product-by-id/{id}")
    @Operation(summary = "Update Product By Id")
    public  ResponseEntity<ProductResponse<Product>> updateProductById(@RequestBody ProductRequest productRequest, Integer productId){
        Integer updateProduct =  productService.updateProduct(productRequest,productId);
        if(updateProduct != null){
            ProductResponse<Product> response  =  ProductResponse.<Product>builder()
                    .message("Update Product Successfully")
                    .payload(productService.getProductById(updateProduct))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }else{
            ProductResponse<Product> response  =  ProductResponse.<Product>builder()
                    .message("Sorry you can't update this Id ")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }
    }
}
