package com.example.homework2.repository;

import com.example.homework2.model.entity.Product;
import com.example.homework2.model.request.CustomerRequest;
import com.example.homework2.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("SELECT * FROM products ORDER BY id ASC")
    List<Product> getAllProducts();

    @Select("SELECT * FROM products WHERE id = #{productId}")
    Product  getProductById(Integer productId);

    @Delete("DELETE FROM products WHERE id = #{productId}")
    boolean deleteProductById(Integer productId);

    @Select("INSERT INTO products (pro_name, pro_price) VALUES(#{request.name}, #{request.price}) "
            +"RETURNING id")
    Integer addNewProduct(@Param("request") ProductRequest productRequest);

    @Select("UPDATE products " +
            "SET pro_name = #{request.name}, " +
            "pro_price = #{request.price} " +
            "WHERE id = #{productId} " +
            "RETURNING id")

    Integer updateProduct(@Param("request") ProductRequest productRequest, Integer productId);

     @Select("SELECT DISTINCT p.id , p.pro_name, p.pro_price FROM products p " +
         "INNER JOIN invoice_details d ON p.id = d.product_id " +
         "WHERE d.invoice_id = #{invoiceId}")
     List<Product> getProductByInvoiceId(Integer invoiceId);

}
