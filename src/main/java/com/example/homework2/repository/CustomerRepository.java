package com.example.homework2.repository;

import com.example.homework2.model.entity.Customer;
import com.example.homework2.model.request.CustomerRequest;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CustomerRepository {

    @Select("SELECT  * FROM customers ORDER BY id ASC")

    List<Customer>  getAllCusomer();

    @Select("SELECT * FROM customers WHERE id = #{customerId}")
    Customer getCustomerById(Integer customerId);

    @Delete("DELETE FROM customers WHERE id = #{customerId}")
    boolean deleteCustomerById(Integer customerId);

    @Select("INSERT INTO customers (cus_name, cus_address, cus_phone) VALUES(#{request.name}, #{request.address} , #{request.phone})"
    +"RETURNING id")
    Integer addNewCustomer(@Param("request") CustomerRequest customerRequest);

    @Select("UPDATE customers " +
            "SET cus_name = #{request.name}, " +
            "cus_address = #{request.address}, " +
            "cus_phone = #{request.phone} " +
            "WHERE id = #{customerId} " +
            "RETURNING id")

        Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer customerId);

}
