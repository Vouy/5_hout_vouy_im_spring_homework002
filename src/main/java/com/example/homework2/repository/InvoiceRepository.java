package com.example.homework2.repository;

import com.example.homework2.model.entity.Invoice;
import com.example.homework2.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {

 @Select("INSERT INTO invoices (invoice_date , customer_id) VALUES(#{request.date}, #{request.customer_id}) " +
         "RETURNING id")
 Integer addInvoice(@Param("request") InvoiceRequest invoiceRequest);

 @Select("INSERT INTO invoice_details (invoice_id , product_id) VALUES(#{invoiceID}, #{productID})")
 Integer addInvoiceDetail(Integer invoiceID, Integer productID);

 @Select("SELECT * FROM invoices ORDER BY id ASC")
 @Result(property = "id" , column = "id")
 @Result(property = "customer", column = "customer_id",
     one = @One(select = "com.example.homework2.repository.CustomerRepository.getCustomerById")
 )
 @Result(property = "productList", column = "id",
     many = @Many(select = "com.example.homework2.repository.ProductRepository.getProductByInvoiceId")
 )
 List<Invoice> getInvoice();

 @Select("SELECT * FROM invoices  WHERE id = #{invoiceId}")
 @Result(property = "id" , column = "id")
 @Result(property = "customer", column = "customer_id",
         one = @One(select = "com.example.homework2.repository.CustomerRepository.getCustomerById")
 )
 @Result(property = "productList", column = "id",
         many = @Many(select = "com.example.homework2.repository.ProductRepository.getProductByInvoiceId")
 )
 Invoice getInvoiceById(Integer invoiceId);

 @Delete("DELETE FROM invoices  WHERE id = #{invoiceId} ")
 boolean deleteInvoiceById(Integer invoiceId);


 @Select("UPDATE  invoices " +
         "SET invoice_date = #{request.date}, " +
         "customer_id = #{request.customer_id} " +
         "WHERE  id = #{invoiceId} RETURNING id")
 Integer updateInvoice(@Param("request") InvoiceRequest  invoiceRequest ,Integer invoiceId);


 @Select("UPDATE  invoice_details " +
         "SET product_id = #{productId} " +
         "WHERE invoice_id =  #{invoiceId} ")
 void updateProductIdByInvoiceIdInTableInvoiceDetail(Integer invoiceId, Integer productId);

 @Delete("DELETE  FROM  invoice_details WHERE invoice_id = #{invoiceId}")
 void deleteProductBeforeUpdate(Integer invoiceId);

}



